
/* eslint-disable */
exports.install = function(Vue,options){

	Vue.prototype.$getLocalTimeZone = function(dt){
		return this.$moment(dt).tz(this.timezone).format('DD/MM/YYYY HH:mm:ss')
	}
	Vue.prototype.$copy_selection = function(){
		document.execCommand('copy')
	}
	Vue.prototype.$title_tag = function(){
		document.title = ` Aprendiendo Español ` 
	}
	Vue.prototype.timezone = process.env.VUE_APP_TIMEZONE
	Vue.prototype.developer = "Erick avalos"
	Vue.prototype.$time_to_load_list = 1200
	Vue.prototype.$dev_mode = process.env.NODE_ENV === 'development'
	Vue.prototype.$permission_message = "No tienes permiso para visualizar estos registros"

	Vue.prototype.$title_tag()

}
