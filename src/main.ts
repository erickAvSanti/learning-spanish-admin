import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'

Vue.config.productionTip = false

import BootstrapVue from 'bootstrap-vue'

Vue.use(BootstrapVue)

import { library } from '@fortawesome/fontawesome-svg-core'
import { 
	faClone,
	faSave,
	faListOl,
	faHandPointer,
	faChevronRight,
	faChevronLeft,
	faCalculator,
	faAngleDoubleLeft,
	faAngleDoubleRight,
	faArrowUp,
	faArrowDown,
	faBars, 
	faTimes, 
	faEdit, 
	faPlus, 
	faMinus, 
	faMinusCircle, 
	faSync, 
	faMoon, 
	faSun, 
	faLink, 
	faExternalLinkAlt, 
	faCarSide, 
	faUserCheck, 
	faUserTie, 
	faCoins, 
	faFolderOpen, 
	faKey, 
	faCommentDollar, 
	faUsers,
	faUser,
	faCarCrash,
	faHouseDamage,
	faFileInvoice,
	faSearch,
	faSearchPlus,
	faSearchMinus,
	faEye,
	faEyeSlash,
	faBan,
	faCheckCircle,
	faTimesCircle,
	faMusic,
	faFileAlt,
	faFileWord,
	faFileVideo,
	faFilePowerpoint,
	faFilePdf,
	faFileArchive,
	faFileImage,
	faImage,
	faUpload,
	faCopy,
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(
	faClone,
	faSave,
	faListOl,
	faHandPointer,
	faArrowUp,
	faArrowDown,
	faCalculator,
	faChevronRight,
	faChevronLeft,
	faAngleDoubleLeft,
	faAngleDoubleRight,
	faBars, 
	faTimes, 
	faEdit, 
	faPlus, 
	faMinus, 
	faMinusCircle, 
	faSync, 
	faMoon, 
	faSun, 
	faLink, 
	faExternalLinkAlt, 
	faCarSide, 
	faUserCheck, 
	faUserTie, 
	faCoins, 
	faFolderOpen, 
	faKey, 
	faCommentDollar, 
	faUsers,
	faUser,
	faCarCrash,
	faHouseDamage,
	faFileInvoice,
	faSearch,
	faSearchPlus,
	faSearchMinus,
	faEye,
	faEyeSlash,
	faBan,
	faCheckCircle,
	faTimesCircle,
	faMusic,
	faFileAlt,
	faFileWord,
	faFileVideo,
	faFilePowerpoint,
	faFilePdf,
	faFileArchive,
	faFileImage,
	faImage,
	faUpload,
	faCopy,
)

Vue.component('font-awesome-icon', FontAwesomeIcon)

import axios from 'axios' 
import VueAxios from 'vue-axios'

Vue.use(VueAxios, axios)

/*
const moment = require('moment')
require('moment/locale/es')
 
Vue.use(require('vue-moment'), {
    moment
})
*/

import VueMoment from 'vue-moment'
import moment from 'moment-timezone'
 
Vue.use(VueMoment, {
    moment,
})

import 'bootstrap/dist/css/bootstrap.css';
import 'pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.css'

Vue.use( require('@ckeditor/ckeditor5-vue') );

Vue.use(require('@/plugins/index.js'))

import Notifications from 'vue-notification'
Vue.use(Notifications)

import device from "vue-device-detector"
Vue.use(device)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
