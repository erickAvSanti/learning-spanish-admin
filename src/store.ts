import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  	modals:{
  		login:false,
  	},
    user:{

    },
  },
  mutations: {
  	showModalLogin(state){
  		state.modals.login = true
  	},
  	hideModalLogin(state){
  		state.modals.login = false
  	},
  	setModalLoginState(state,value){
  		state.modals.login = value
  	},
    setUser(state,value){
      state.user = value
    },
  },
  actions: {

  }
})
