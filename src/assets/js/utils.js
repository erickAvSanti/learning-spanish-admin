
export function pag(current,total){
  let arr = []
  var push2=function(_arr,val){
    var found = false
    for(var i=0;i<_arr.length;i++){
      if(_arr[i]==val && val!='...'){
        found=true
        break
      }
    }
    if(!found)_arr.push(val)
    return found
  };
  if(total>7){
    
    if(current<5){
      for(var i=1;i<5;i++){
        push2(arr,i)
      }
      push2(arr,5)
      push2(arr,'...')
      push2(arr,total-2)
      push2(arr,total-1)
      push2(arr,total)
    }else if(current>total-4){
      push2(arr,1)
      push2(arr,2)
      push2(arr,3)
      push2(arr,4)
      push2(arr,'...')
      for(var i=total-4;i<=total;i++){
        push2(arr,i)
      }
    }else{
      push2(arr,1)
      push2(arr,2)
      push2(arr,3)
      push2(arr,4)
      push2(arr,'...')
      push2(arr,current-1)
      push2(arr,current)
      push2(arr,current+1)
      push2(arr,'...')
      push2(arr,total-2)
      push2(arr,total-1)
      push2(arr,total)
    }
  }else{
    for(var i=1;i<=total;i++){
      push2(arr,i)
    }
  }
  var removeIndex = []
  for(var i=0;i<arr.length-2;i++){
    var v1 = arr[i]
    var v2 = arr[i+1]
    var v3 = arr[i+2]
    if(typeof v1=='number' && typeof v3=='number' && v2=='...'){
      if(v3-v1==1){
        removeIndex.push(i+1)
      }
      
    }
  }
  for(var i=arr.length-1;i>=2;i--){
    var v1 = arr[i-2]
    var v2 = arr[i-1]
    var v3 = arr[i]
    if(typeof v1=='number' && typeof v3=='number' && v2=='...'){
      if(v3-v1==1){
        removeIndex.push(i-1)
      }
      
    }
  }
  for(var i=0;i<arr.length;i++){
    var midd = arr[i]
    var v_prev = arr[i-1]
    var v_next = arr[i+1]
    if(typeof v_prev=='number' && typeof v_next=='number' && midd=='...'){
      if(v_next-v_prev==2){
        arr[i] = v_prev+1
      }
      
    }
  }
  if(removeIndex.length>0){
    var arr2 = []
    for(var i=0;i<arr.length;i++){
      var ff = false
      for(var j=0;j<removeIndex.length;j++){
        if(i==removeIndex[j]){
          ff=true
          break
        }
      }
      if(!ff){
        arr2.push(arr[i])
      }
    }
    arr = arr2
  }
  return arr
}
export function get_inputs_flag_edition(arr){
  let arr2 = new Array(arr.length)
  let _length = null
  let pos = 0
  for(let objx of arr){
    let json = {}
    for(let ojby in objx){
      if(typeof ojby == 'number' || typeof ojby == 'string'){
        json[ojby] = false
      }
    }
    arr2[pos] = json
    pos++
  }
  console.log(arr2)
  return arr2
}