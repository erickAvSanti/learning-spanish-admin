export function parse_errors(data){
	if(!data || !data.details)return null
	let details = data.details
	/*
	let str = "<ul class='parse_errors'>"
	for(let idx in details){
		let detail = details[idx]
		str += `<li>`
		str += `<label>${idx}</label><ul>`
		for(let idy in detail){
			str += `<li>${detail[idy]}</li>`
		}
		str += `</ul></li>`
	}
	str +='</ul>'
	*/
	let str = "<div class='parse_errors'>"
	let any = false
	for(let idx in details){
		let detail = details[idx]
		if(Array.isArray(detail)){
			for(let idy in detail){
				if(/password/.test(idx) && idy>0)continue
				let d = detail[idy]
				if(!d || d=='')continue
					any = true
				str += `<div>${d}</div>`
			}
		}else{
			if(detail && detail!='')str += `<div>${detail}</div>`
		}
	}
	str +='</div>'
	if(!any)return null
	return str
}