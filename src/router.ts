import Vue from 'vue'
import Router from 'vue-router'
import Usuarios from './views/Usuarios.vue'
import UsuariosAreas from './views/UsuariosAreas.vue'
import UsuariosCivilStatus from './views/UsuariosCivilStatus.vue'
import UsuariosDocTypes from './views/UsuariosDocTypes.vue'
import SpanishVerbs from './views/SpanishVerbs.vue'
import SpanishMaths from './views/SpanishMaths.vue'
import SpanishAnimals from './views/SpanishAnimals.vue'
import SpanishHouseObjects from './views/SpanishHouseObjects.vue'
import GuessWords from './views/GuessWords.vue'
import CompletePhrases from './views/CompletePhrases.vue'
import Login from './views/Login.vue'


Vue.use(Router)

export default new Router({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/usuarios',
      name: 'Usuarios',
      component: Usuarios,
    },
    {
      path: '/areas-usuarios',
      name: 'UsuariosAreas',
      component: UsuariosAreas,
    },
    {
      path: '/estado-civil-usuarios-clientes',
      name: 'UsuariosCivilStatus',
      component: UsuariosCivilStatus,
    },
    {
      path: '/tipos-documentos-usuarios',
      name: 'UsuariosDocTypes',
      component: UsuariosDocTypes,
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
    },
    {
      path: '/complete-phrases',
      name: 'CompletePhrases',
      component: CompletePhrases,
    },
    {
      path: '/guess-words',
      name: 'GuessWords',
      component: GuessWords,
    },
    {
      path: '/',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    }
  ]
})
